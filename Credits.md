# Credits
This file contains all the people who've ever contributed to MRCL.

### Note
**This credit file is often not up to date. Make a merge request or an issue if the information is not accurate.**


## Builds
### Old Kadic:  [Planet Minecraft](https//www.planetminecraft.com/project/codelyokokadicacademy/)

### Refurbished old Kadic
* Alexmario5 
* Ten 

### New Kadic (base) 
* KaruzoHikari 
* GoodOldJack12

### New Kadic (decoration) 
* KaruzoHikari 
* TankadiN 
* GoodOldJack12
* Luclyoko 

### New factory
* TankadiN 
* TheLeech 
* KaruzoHikari 

### Carthage 
* Noojin 
* Ten 
### Ice sector 
* Noojin 
* Ten 
### Forest replica and derivatives 
* Noojin 
* Ten 
## Gameplay

### Scanner transition 
* Jack (code)
* Noojin (colors)
* Alexmario5 (redstone)
* Ten (redstone)
* KaruzoHikari (code)
### Scanners
* KaruzoHikari (Animations, models, code)
* GoodOldJack12 (code)

### Old Superscan system (and general SC redstone) 
* Ten 
### Old Scanner system 
* Ten 

### Factory door
* KaruzoHikari 
* Alexmario5 
* Ten

### Advancements
* Alexmario5 
* KaruzoHikari 

### Frontier
* Alexmario5 

### Old devirt system
* Alexmario5 

### Transporter
* Ten 
* Alexmario5 
* KaruzoHikari 
##  Code

### Project Carthage 
(rttp, devirt, superscan frontend, tower activation front end)
* GoodOldJack12
* KaruzoHikari 
### Project XANA (more attacks)
* Sky
### Routines
* KaruzoHikari
### Pathfinder for routines
* Matty
### Elevator plugin
* KaruzoHikari
### Misc plugins
(Keys, chairs, blinds, computers,..)
* KaruzoHikari
### Website
* Ten
* Ivo

## Assets

### Texture pack 
* Ten 
* Alexmario5 
* Noojin 
* Franz hopper 

### Skins
* Ramen Noodiles
  - Jeremie Belpois
  - Aelita Stones
  - Ulrich Stern
  - Odd Della Robbia
  - Yumi Ishiyama
  - William Dunbar
  - Elizabeth "Sissi" Delmas
  - Samantha Knight
  - Milly Solovieff
  - Tamiya Diop
* Solar 
  - Giles Fumet
  - Brigite Meyer
  - Gustave Chardin
  - Rachael Kensington
  - Gaston Lagrange
  - Hans Klotz
  - Nicole Weber
  - Ninja
  - Suzanne Hertz
* KaruzoHikari
  - Suzanne Hertz
* AdminProductions
  - Jim Morales
  - Jean Pierre Delmas
  - Rosa Petitjean
  - Yolanda Perraudin

### Misc sounds
* Juleic1123 

### Server logo(s)
* TankadiN 

### Translations
 * French 
   - Alexmario5 
 * Spanish 
   - KaruzoHikari 
 * Finnish
   - Ten
 * Italian 
   - 3DGamer 
 * Romanian
   - Survinar
 * Polish
   - MATRIX
 * Translation code
   - KaruzoHikari

A huge thanks to all the Alpha Testers and to Sky for financially backing the project,
and to our supporters on Patreon and Discord as well ^^ 

Want your name here? apply for staff today!
