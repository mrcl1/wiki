[Français](/Translated/French/Rules/rules.md) | [Español](/Translated/Spanish/Rules/NamingRules.md) | [Romanian](/Translated/Romanian/Rules/NamingRules.md)

---
# Naming rules
We have a very specific set of rules for usernames on both discord and minecraft.

## Names that are not allowed
You can't have a username that:
* Resembles the name of a character in the show
* Contains no identifyable set of normal characters
* Can't be referenced to as if it's a name
* Can't be mentioned easily on Discord

## What to do if you have a banned name
It depends. Here's a few scenarios:

### The banned name is your minecraft username
If you have a legitimate copy of minecraft, and you don't want to change your username through mojang.com, ask a moderator to change your nickname to something else.

**If you don't own minecraft, change your name in the launcher or you *will* get banned**

### The banned name is your discord username
If you don't want to change your discord username, use the nickname feature (or we will do it for you, and you won't like what we pick)

## Examples of banned names
### 11ε ƉơƈŧēůƦ
Not allowed because of the lack of normal characters
### Aelita_Shaeffer
Not allowed because it's the name of a CL character
### Jeremie
Same as above. If your name is actually Jeremie, talk to the mods.
### gksqzeoKKSdqslAZRf
Not allowed because it can't be used as if it's a name. You can't expect us to say "hey, gksqzeoKKSdqslAZRf, how are you doing?"

