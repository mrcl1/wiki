# [CHECK THE WIKI FOR UPDATED RULES](https://wiki.mrcl.cc/en/Rules)

# [Français](/Translated/French/Rules/Rules.md) | [Español](/Translated/Spanish/Rules/Rules.md) | [Romanian](/Translated/Romanian/Rules/Rules.md)
# Rules
The rules in this document apply to the discord and the minecraft server.
They are a guideline, and in the end Admins and Moderators have the final call.
They're also listed in no particular order, except The First Rule

## The First Rule
The first and most important rule is: **use common sense**. 

Don't be a nuisance to others, and be a decent human being. Don't make us add a rule for your sake just because we overestimated human decency.

## Global rules

- Moderation decisions are final. If you wish to complain, contact an admin, or Jack if you wish to complain about an admin.

- Don't spoil advancement locations. You can help each other, just give people the opportunity to find stuff themselves.

- Spam is not allowed. Depending on the channel, this rule is enforced differently. See [Channel Usage](ChannelUsage.md) for more details.

- You may argue over just about anything, but we draw the line at personal insults. If you want to hold a roast fest, do it in DM's

- Racism, homophobia, sexism of any kind of discriminatory act is not something we tolerate. 

- We have no bans on swearing, but use your common sense and don't overdo it.

- Any attempt at impersonation of another person within the community will result in you and any alternate accounts you hold being banned.

- Abuse to any community member or those within a group will be met with moderation action against you, any release of private personal information will result in your accounts being permanently removed from all MRCL servers.

- You may **only** advertise Code Lyoko related projects within reason (excluding any financial links such as Patreon or Ko-Fi). This is up to moderator discretion. If you wish to advertise some other project, ask an admin first.

- **We have restrictions on usernames and nicknames, which you can read about [here](NamingRules.md)**

- The common language is English, but other languages may be spoken in-game and in [specific channels](ChannelUsage.md#Language_channels)

- Alt accounts are not allowed.

- Each channel must be kept relevant *duh* (Aka #outofcontext is used for memes, pictures and quotes taken out of context, not more general discussions)

- Nudity and porn is not allowed, along with any attempt at "Erotic Roleplay". Jokes and memes are allowed to a certain degree in the appropriate [channels](ChannelUsage.md)

- Ban evasion will get you...banned again. Permanently. Seems pretty logical yet I have to write this rule.

- **Being a dick to other people in DM's or other servers might also get you banned here**
## Discord-specific rules

- Exessively mentioning/@'ing anyone without cause will get you muted, or worse.

- Trolling/annoying people with bots is not allowed. If someone complains, you will always be in the wrong. 

- English is to be spoken in all channels except the designated language channels, the reverse also applies where Englishs shouldn't be spoken in the language channels.

- Use of names or nicknames composed of characters not found on a standard QWERTY keyboard will result in your username being set to the easiest translation.

## Minecraft-specific rules
- Trolling by using game mechanics is not allowed. Examples include, but are not limited to: 
    * Changing the scanner destination of someone without asking
    * Spamming the Return To The Past
    * Repeatedly pvp-ing against someone's will while not xanafied
- The use of modded clients (excluding Optifine), hacks or exploits is not tolerated under any circumstances and will result in a permanent ban from the server.
