# [English](/Rules/Rules.md) | [Español](/Translated/Spanish/Rules/Rules.md) | [Romanian](/Translated/Romanian/Rules/Rules.md)

# Règles
Les règles de ce document s'appliquent sur le serveur Discord et le serveur Minecraft.
Ce sont une ligne directrice, les modérateurs et les administrateurs feront le choix final.
Elles ne sont listées dans aucun ordre en particulier sauf la première règle.

## La Première Règle
La première règle, la plus importante, est : **soyez logiques**.

N'allez pas embêter les autres inutilement, soyez un humain normal. Nous espérons ne pas rajouter une règle car nous avons surestimé le cerveau humain.

## Règles Globales

- Les décisions de la modération sont définitives. Si vous voulez vous plaindre, contactez un administrateur ou contactez Jack si vous voulez porter plainte sur un administrateur.

- **Dire à quelqu'un où se trouve un succès (advancement) ou comment faire un succès n'est pas autorisé.** Cependant, vous pouvez expliquer les mécaniques de bases du jeu. Une liste de tout ce qui est autorisé à expliquer ou non se trouve [ici](/Translated/French/Rules/AdvancementRules.md)

- Le spam/abus n'est pas autorisé. Selon le salon, cette règle est appliquée de façon différente. Allez voir l'[Utilisation des salons](/Translated/French/Rules/ChannelUsage.md) pour plus de détails.

- Vous pouvez débattre sur presque tout, cependant nous intervenons s'il y a des insultes envers les personnes. Si vous voulez régler vos problèmes, allez en MP.

- Le racisme et la discrimination ne sont en aucun cas acceptés.

- Nous ne bannissons pas pour des insultes tant que celles-ci ne dépassent pas la limite des insultes personnelles.

- Vous ne pouvez **uniquement** que faire de la pub de façon raisonnable pour des projets en rapport avec Code Lyoko. Ceci est à la discrétion des modérateurs. Si vous voulez faire de la pub pour un autre projet en dehors de Code Lyoko, demandez à un adminisatrateur.

- **Nous avons des restrictions sur les pseudos et les surnoms, vous pouvez les lire [ici](Translated/French/Rules/NamingRules.md)**

- La langue la plus utilisée est l'anglais mais nous avons une communauté qui parle d'autres langues, que ce soit en jeu ou dans les [salons spécifiques](Translated/French/Rules/ChannelUsage.md#Language_channels)

- Les doubles comptes ne sont pas autorisés.

- Les contenus du type "NSFW" comme la nudité ou le porno ne sont pas autorisés. Les memes et les blagues sont acceptés à un certain degré dans les [salons](ChannelUsage.md) appropriés.

- Si vous essayez d'échapper à un ban, alors vous serez bannis à nouveau. Pour toujours.

- **Embêter les autres personnes en MP ou sur les autres serveurs peut valoir un ban sur notre serveur.**

## Règles spécifiques à Discord

- Mentionner une personne ou un rôle excessivement sans raison sera suivi d'un mute, voir pire.

- Troller/Embêter les gens avec des bots n'est pas autorisé. Si quelqu'un se plaint, vous serez en tort.

## Règles spécifiques à Minecraft

- Troller en utilisant les mécaniques de jeu n'est pas autorisé. Quelques exemples, qui ne représentent pas la liste complète : 
    * Changer la destination du scanner d'une personne sans demander,
    * Abuser du Retour vers le passé,
    * Combattre quelqu'un en boucle qui ne souhaite pas se battre en n'étant pas xanatifié.

- L'utilisation de clients modifiés (à part Optifine), de hacks ou l'exploitation de bugs n'est en aucun cas tolérée et peut se solder par un bannissement définitif du serveur.
