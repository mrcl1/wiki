[English](/Rules/ChannelUsage.md) | [Français](/Translated/French/Rules/ChannelUsage.md) | [Romanian](/Translated/Romanian/Rules/ChannelUsage.md)

---
# Uso de los Canales
Aquí puedes encontrar para qué sirven nuestros canales en nuestro [Servidor de Discord](https://discord.mrcl.cc).

**Si quieres hablar en un idioma que no sea el inglés, haz click [aquí](#Canales-de-idiomas) para encontrar los canales para eso**


# Categoría general
### \#general
El canal general está pensado para conversaciones sobre temas más serios. Shitposting no está permitido, y los memes solo están permitidos si cuadran con el tema del momento. No hay ninguna norma que se aplique fuertemente aquí, simplemente no publiques un meme random mientras la gente está teniendo una conversación seria.

### \#sewers
Sewers está pensado para shitposting y memes, o links random como "Hey este post de Reddit es bastante guay". Esto no significa que tengas permitido spammear, pero eres más libre en este canal.

### \#bot-spam
Bot-spam está pensado para jugar con los bots. Este canal no está moderado regularmente, así que llama a un moderador si la gente no se comporta. Dicho eso, este canal tiene bastante spammy por su naturaleza, así que la norma de "no spam" no se aplica demasiado.

### \#voice-chat
Este canal es para los que quieren interactuar, mediante texto, con aquellos que están hablando por un chat de voz. No uses bots de música aquí, haz eso en #bot-spam

### \#suggestions
Este canal es para publicar tus sugerencias para el servidor de Discord, o el servidor de Minecraft. Ten en cuenta que tus ideas probablemente hayan sido sugeridas antes, y que puede que ignoremos completamente algunas de ellas. Simplemente es un lugar divertido para hacer una lluvia de ideas.

# Canales de idiomas
En MRCL, estamos abiertos a las personas que hablan otros idiomas. Por ahora, tenemos estos canales disponibles:
- \#fr-talk
- \#es-talk
- \#fi-talk

Estos canales son para hablar en sus respectivos idiomas, o para preguntar dudas sobre esos idiomas (por ejemplo: "¿Cómo digo "baguette" en francés?")

Si quieres un canal para tu idioma, contacta con un admin y lo consideraremos.

# Categoría miscelánea
No todos los canales de esta categoría aparecerán aquí, solo aquellos con reglas específicas

### \#head-area-exposing-channel
Este canal es para subir selfies tuyos. No subas nudes de ti mismo (o de otra persona) y no publiques selfies falsos. Simplemente no es divertido. **Tampoco puedes subir un selfie de otra persona.**

### \#not-safe-for-kids
**Porno y nudismo no están permitidos en este canal.**

NSFK es un canal donde puedes subir tus memes con tonos más oscuros o contenido que no es apropiado para, por ejemplo, gente de 14 años. También puedes hablar de temas más sensibles como sexualidad, política o género aquí. (Ten en cuenta que estos temas no están baneados en otros canales de por sí, pero este es el canal más adecuado para ellos).

### \#mrclmemes
Este canal es **solo para memes**. 
**No** comentes en este canal sobre los memes publicados. Esto es simplemente para archivar todos los memes relacionados con MRCL en un mismo canal.

### \#free-games
Este canal es donde puedes publicar juegos que son **gratis para quedártelos** por un tiempo limitado. Tenemos un bot que ocasionalmente publica links de eso aquí también. (no somos responsables del contenido que el bot publica)

### \#screenshots
Este canal es donde puedes publicar tus mejores screenshots de MRCL. **Comentarlas aquí no está permitido.**
Este no es un canal para reportar bugs, ese es #bug-report.

Si te gusta una screenshot, puedes reaccionar con el emoji :pushpin:, y lo anclaremos si suficientes personas también sienten que debería estar anclado.

Puede ser que usemos estas screenshots en nuestra web u otras redes sociales también.