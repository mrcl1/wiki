# [English](/Rules/Rules.md) | [Français](/Translated/French/Rules/Rules.md) | [Romanian](/Translated/Romanian/Rules/Rules.md)

---
# Reglas
Las reglas en este documento se aplican tanto al Discord como al servidor de Minecraft.
Son una guía, al final son los Admin y Moderadores los que tienen la palabra final.
No están escritas en ningún orden en particular, excepto La Primera Regla

## La Primera Regla
La primera y más importante regla es: **usa el sentido común**. 

No molestes a los demás, y sé una persona decente. No nos hagas añadir una regla por tus acciones simplemente porque habíamos sobreestimado la decencia humana.

## Reglas globales

- Las decisiones de Moderación son definitivas. Si quieres quejarte, contacata a un admin, o a Jack si quieres quejarte sobre un admin.

- **Decirle a alguien (o preguntar) sobre dónde o cómo conseguir un logro no está permitido.** Aún así, puedes explicar mecánicas de juego normales. Una lista completa de lo que está y no está permitido se puede encontar [aquí](AdvancementRules.md)

- El Spam no está permitido. Dependiendo del canal, esta regla es aplicada de forma diferente. Lee [Uso de los Canales](ChannelUsage.md) para más detalles.

- Puedes discutir sobre cualquier cosa, pero el límite lo ponemos en insultos personales. Si quieres montar un festival de humillaciones, hazlo en DM's

- Racismo o discriminación de cualquier tipo no está tolerado. 

- No hacemos ban sobre mal lenguaje o palabrotas, pero usa el sentido común y no te pases con él.

- Puedes publicitar **solo** proyectos relacionados con Código Lyoko, dentro de lo razonable. Esto está a discreción de los moderadores. Si quieres publicitar un proyecto diferente, pregunta a un Admin primero.

- **Tenemos restricciones en nombres de usuario y nicks, las cuales puedes leer [aquí](NamingRules.md)**

- El idioma común es el Inglés, pero otros idiomas pueden ser hablados en el juego y en [canales específicos](ChannelUsage.md#Language_channels)

- Cuentas secundarias no están permitidas.

- Contenido NSFW como nudismo o porno no están permitidos. Memes y bromas están permitidas hasta cierto punto en los [canales apropiados](ChannelUsage.md)

- Si evades un ban... volverán a banearte. Permanentemente. Parece bastante lógico, pero aún así tenemos que escribir esta regla.

- **Ser un imbécil con los demás en DM's u otros servidores también puede hacer que seas baneado aquí.**
## Reglas específicas de Discord

- Mencionar/"hacer @" excesivamente a alguien, sin ninguna razón, puede hacer que acabes muteado o peor.

- Trollear / molestar a la gente con los bots no está permitido. Si alguien se queja, siempre vas a estar equivocado. 

## Reglas específicas de Minecraft
- Trollear usando las mecánicas del juego no está permitido. Algun ejemplos incluyen (sin estar limitados a ellos): 
    * Cambiar el destino de los escáneres de alguien sin preguntar
    * Spammear la Vuelta al Pasado
    * Hacer PVP repetidamente con alguien que no quiere, cuando no se está xanaficado
